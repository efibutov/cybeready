from django.contrib import admin
from django.urls import include, path


urlpatterns = [
    path('bazs/', include('baz_scheduler.urls')),
    path('admin/', admin.site.urls),
]
