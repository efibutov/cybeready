from django.urls import include, path
from . import views


app_name = 'baz_scheduler'
urlpatterns = [
    path('', views.index, name='index'),
    path('baz/<int:pk>/', views.BazDetailView.as_view(), name='baz-details'),
    path('baz/create/', views.BazCreateView.as_view(), name='baz-create'),
    path('baz/update/<int:pk>/', views.BazUpdateView.as_view(), name='baz-update'),
    path('baz/delete/<int:pk>/', views.BazDeleteView.as_view(), name='baz-delete'),
    path('baz/thanks/', views.baz_thanks, name='baz-thanks'),

    path('sent_baz/<int:pk>/', views.SentBazDetailView.as_view(), name='sent-baz-details'),
    path('sent_baz/create/', views.SentBazCreateView.as_view(), name='sent-baz-create'),
    path('sent_baz/update/<int:pk>/', views.SentBazUpdateView.as_view(), name='sent-baz-update'),
    path('sent_baz/delete/<int:pk>/', views.SentBazDeleteView.as_view(), name='sent-baz-delete'),
    path('sent_baz/thanks/', views.sent_baz_thanks, name='sent-baz-thanks'),

    path('employee/<int:pk>/', views.EmployeeDetailView.as_view(), name='employee-details'),
    path('employee/create/', views.EmployeeCreateView.as_view(), name='employee-create'),
    path('employee/update/<int:pk>/', views.EmployeeUpdateView.as_view(), name='employee-update'),
    path('employee/delete/<int:pk>/', views.EmployeeDeleteView.as_view(), name='employee-delete'),
    path('employee/thanks/', views.employee_thanks, name='employee-thanks'),

    path('customer/<int:pk>/', views.CustomerDetailView.as_view(), name='customer-details'),
    path('customer/create/', views.CustomerCreateView.as_view(), name='customer-create'),
    path('customer/update/<int:pk>/', views.CustomerUpdateView.as_view(), name='customer-update'),
    path('customer/delete/<int:pk>/', views.CustomerDeleteView.as_view(), name='customer-delete'),
    path('customer/thanks/', views.customer_thanks, name='customer-thanks'),

    path('api-auth/', include('baz_scheduler.rest_urls', namespace='rest_framework'))
]
