from django.apps import AppConfig
from threading import Thread
# from .models import Baz, SentBaz, Employee, Customer

# def send_bazs():
#     for customer in Customer.objects.all():
#         for employee in customer.employee_set.all():
#             for baz in employee.baz_set.all():
#                 print(baz)
#     for baz in Baz.objects.all()


class BazSchedulerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'baz_scheduler'

    def run():
        import os
        import time
        import datetime

        while True:
            print(f'\nBazSchedulerConfig\nPID = {os.getpid()}, {datetime.datetime.now()}')
            time.sleep(1.0)

    def ready(self):
        import os
        # If The app is already running, do not start thread with scheduler
        if os.environ.get('RUN_MAIN', None) != 'true':
            return

        t = Thread(target=BazSchedulerConfig.run)
        t.start()
