from django.contrib import admin
from .models import Baz, Customer, Employee, SentBaz


class BazAdmin(admin.ModelAdmin):
    pass


class CustomerAdmin(admin.ModelAdmin):
    pass


class EmployeeAdmin(admin.ModelAdmin):
    pass


class SentBazAdmin(admin.ModelAdmin):
    pass


admin.site.register(Baz, BazAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(SentBaz, SentBazAdmin)
