from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Baz, SentBaz, Customer, Employee
from django.contrib.auth.models import User, Group


class BazSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Baz
        fields = ['name', 'date_created', 'subject', 'content', 'send_interval']


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ['name', 'manager_email']


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ['customer', 'email', 'first_name', 'second_name', 'sent_baz']


class SentBazSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SentBaz
        fields = ['employee', 'baz', 'sent_on', 'sent']


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'username', 'email', 'groups']


# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ['url', 'name']


# class Serializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ['url', 'name']
