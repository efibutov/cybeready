from django.test import TestCase

from django.test import TestCase
from ..models import Baz, SentBaz, Employee, Customer


class TestBazCreate(TestCase):
    def test_create_baz(self):
        customer = Customer.objects.create(name='C1', manager_email='c1@c1.com')
        customer.save()
        employee = Employee.objects.create(customer=customer,  email='employee@c1.com')
        new_baz_name = 'new baz'
        baz = Baz.objects.create(
            name=new_baz_name,
            scheduling_interval=1
        )
        baz.target.set((employee,))
        baz.save()
        self.assertEqual(baz.name, new_baz_name)
