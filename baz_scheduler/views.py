from django.urls import reverse_lazy
from django.http import HttpResponse
from .models import Baz, Customer, Employee, SentBaz
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView


class BazDetailView(DetailView):
    model = Baz

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['obj'] = kwargs['object']
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class BazCreateView(CreateView):
    model = Baz
    fields = ['name', 'subject', 'content', 'send_interval']
    template_name_suffix = '_create_form'
    success_url = reverse_lazy('baz_scheduler:baz-thanks')

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class BazUpdateView(UpdateView):
    model = Baz
    fields = ['name']
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class BazDeleteView(DeleteView):
    model = Baz
    success_url = reverse_lazy('baz_scheduler:baz-thanks')


def baz_thanks(request):
    return HttpResponse('Thanks.')


class CustomerDetailView(DetailView):
    model = Customer

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['obj'] = kwargs['object']
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CustomerCreateView(CreateView):
    model = Customer
    fields = ['name', 'manager_email']
    template_name_suffix = '_create_form'
    success_url = reverse_lazy('baz_scheduler:customer-thanks')

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class CustomerUpdateView(UpdateView):
    model = Customer
    fields = ['name', 'manager_email']
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class CustomerDeleteView(DeleteView):
    model = Customer
    success_url = reverse_lazy(f'baz_scheduler:customer-thanks')

    def delete(self, request, *args, **kwargs):
        print(self.get_object())
        return super().delete(request, *args, **kwargs)


def customer_thanks(request):
    return HttpResponse('Thanks.')


class EmployeeDetailView(DetailView):
    model = Employee

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['obj'] = kwargs['object']
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class EmployeeCreateView(CreateView):
    model = Employee
    fields = ['email', 'first_name', 'second_name', 'customer']
    template_name_suffix = '_create_form'
    success_url = reverse_lazy('baz_scheduler:employee-thanks')

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class EmployeeUpdateView(UpdateView):
    model = Employee
    fields = ['name']
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class EmployeeDeleteView(DeleteView):
    model = Employee
    success_url = reverse_lazy('baz_scheduler:employee-thanks')


def employee_thanks(request):
    return HttpResponse('Thanks.')


class SentBazDetailView(DetailView):
    model = SentBaz

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['obj'] = kwargs['object']
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class SentBazCreateView(CreateView):
    model = SentBaz
    fields = ['customers', 'bazs']
    template_name_suffix = '_create_form'
    success_url = reverse_lazy('baz_scheduler:employee-thanks')

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class SentBazUpdateView(UpdateView):
    model = SentBaz
    fields = ['name', 'manager_email']
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class SentBazDeleteView(DeleteView):
    model = SentBaz
    success_url = reverse_lazy('baz_scheduler:sent-baz-thanks')


def sent_baz_thanks(request):
    return HttpResponse('Thanks.')


def index(request):
    return HttpResponse('Index page')
