# Describe all the entities models and relationships between them

from django.db import models
from django.shortcuts import reverse
from django.utils.timezone import now, datetime
from django.core.validators import MaxValueValidator, MinValueValidator


class Customer(models.Model):
    name = models.CharField(max_length=50, unique=True, null=False, blank=False)
    manager_email = models.EmailField(null=False, blank=False, unique=True)

    def __str__(self):
        return f'{self.name}, ({self.manager_email})'


class Employee(models.Model):
    customer = models.ForeignKey(to=Customer, on_delete=models.CASCADE, blank=False, null=False)
    email = models.EmailField(blank=False, null=False, unique=True)
    first_name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.email}'

    def get_absolute_url(self):
        return reverse('baz_scheduler:employee-details', args=(self.pk,))


class Baz(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)
    subject = models.CharField(max_length=100)
    content = models.TextField(max_length=2000)
    malicious_link = models.URLField(blank=False, null=False)
    start_spreading = models.DateTimeField(default=now, auto_created=True, blank=False, null=False)
    target = models.ManyToManyField(to=Employee, through='SentBaz')
    scheduling_interval = models.PositiveIntegerField(blank=False, null=False, validators=[MinValueValidator(1), MaxValueValidator(4)])
    future_sending_delta = models.DateTimeField(blank=True, null=True)

    # def save(self, *args, **kwargs):
    #     if self.future_scheduled_sending_shift and self.future_scheduled_sending_shift <= self.start_spreading:
    #         raise ValueError('Future sending should be later than beginning')

    #     return super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name} ({self.subject})'

    def get_absolute_url(self):
        return reverse('baz_scheduler:baz-details', args=(self.pk,))


class SentBaz(models.Model):
    employee = models.ForeignKey(to=Employee, on_delete=models.CASCADE, blank=False)
    baz = models.ForeignKey(to=Baz, on_delete=models.CASCADE, blank=False)
    last_sent_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    sent_success = models.BooleanField(default=False, null=False, blank=False)
    opened_by_target = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return f'{self.baz_set[:5]}'

    def get_absolute_url(self):
        return reverse('baz_scheduler:sent-baz-detail', kwargs={'pk': self.pk})

    class Meta:
        # Ensure there is a single "interaction" between Baz and target
        unique_together = (('employee', 'baz',))
