from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.urls import path
from . import views
from rest_framework import viewsets, permissions
from .serializers import (
    BazSerializer, SentBazSerializer, EmployeeSerializer, CustomerSerializer
)
from .models import Baz, SentBaz, Employee, Customer


class BazViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Baz.objects.all()
    serializer_class = BazSerializer
    permission_classes = [permissions.IsAuthenticated]


# class GroupViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = Group.objects.all()
#     serializer_class = GroupSerializer
#     permission_classes = [permissions.IsAuthenticated]


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'username', 'email', 'is_staff']

# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


router = routers.DefaultRouter()
router.register(r'bazs', BazViewSet)


app_name = 'baz_scheduler'
urlpatterns = [
    path('', include(router.urls)),
]
